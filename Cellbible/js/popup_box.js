function popup_box() {
	var last_runner = false;

	/**
	 * invoke box
	 */
	$('.popup_box').on('click', function() {
		$('#popup_box_wrapper').show();
		var $that = $(this);
		
		var $lang_box = $('#language');
		
		var langs = JSON.parse($that.attr('values'));
		
		$lang_box.html('');
		
		for(var lang in langs) {
			$("<li>"+langs[lang]+"</li>").data('data', lang).on('click', function() { // :))))))
				$that.data('data', $(this).data('data')).trigger('change');
			}).appendTo($lang_box);
		}
	});
	
	/**
	 * hide box by click to opacity box
	 */
	$('#popup_box_wrapper').on('click', function(e) {
		if(e.target == this) {
			$(this).hide();
		}
	});
}