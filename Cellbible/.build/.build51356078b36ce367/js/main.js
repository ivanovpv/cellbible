var startTime;
var checkTime;

var supportTouch = $.support.touch
,	scrollEvent = "touchmove scroll"
,	touchStartEvent = supportTouch ? "touchstart" : "mousedown"
,	touchStopEvent = supportTouch ? "touchend" : "mouseup"
,	touchMoveEvent = supportTouch ? "touchmove" : "mousemove"
,	runner_width = 280
,	runner_start = false
,	back_stack = []
,	document_height = $(window).height() - 41;

//Initialize function
var init = function () {
    console.log("start app");
	
	console.log(localStorage);
	
	/**
	 * global func
	 */
	runner(/* slider */);
	popup_box(/* popup boxes */);
	changeLang();
	
	/**
	 * font size
	 */
	if(localStorage['font-size'] != undefined) {
		$('#chapter').css('font-size', localStorage['font-size'] + 'px');
		
		(function() {
			var fs_runner = $('#font_size_runner');
			var steps = fs_runner.data('steps').split(',');
			var step = steps.indexOf(localStorage['font-size']);
			
			console.log();
			
			$('#font_size_runner .runner_button').css('margin-left', (runner_width/steps.length - 5) * (step + 1)).html(localStorage['font-size']);
		})();
	}
	
	/** 
	 * 
	 */
	
	var $main_page_container = $('#main_page');
	var $back_button = $('#back_button');
	var $margin_container = $('#margin_container');
	var $this_page = $main_page_container;
	
	/**
	 * bind links :)
	 */
	$('.pages > li:not(.static)').on('click', function() {
		var $next_page = $('#' + this.getAttribute('data-href'));
		var this_number = parseInt($this_page.attr('number'));
		var next_number = parseInt($next_page.attr('number'));
		var down_number = this_number - next_number;
		
		$next_page.css('visibility', 'visible');
		$this_page.css('visibility', 'hidden');

		$margin_container.css('margin-left', (down_number + 1) * 360).animate({'margin-left':-next_number * 360}, 500, 'ease');
		
		back_stack.push($this_page);
		$this_page = $next_page;
		$back_button.css('display', 'inline-block');
	});
	
	/**
	 * back button
	 */
	function back_button() {
		var $last = back_stack.pop();
		
		var this_number = parseInt($this_page.attr('number'));
		var next_number = parseInt($last.attr('number'));
		var down_number = this_number - next_number;
		
		$this_page.css('visibility', 'hidden');
		$last.css('visibility', 'visible');
		
		$margin_container.css('margin-left', (down_number + 1) * 360).animate({'margin-left':-next_number * 360}, 500, 'ease');
		
		$this_page = $last;
		if(back_stack.length == 0) {
			$(this).css('display', 'none');
		}	
	}
	
	$back_button.on('click', back_button);
	
    $(document).on('tizenhwkey', function(e) {
        if(e.keyName == "back") {
			back_button();
		}
    });
	
	/**
	 * 
	 */
	$('#font_size_runner').on('change', function() {
		localStorage["font-size"] = parseInt($('.runner_button', this).html());
		$('#chapter').css('font-size', localStorage['font-size'] + 'px');
	});
	
	/**
	 * book presets, load heads
	 */

	function setHeads() {
		$.ajaxJSONP({
			'url': 'resources/' + localStorage["language"]  + '/heads.json',
			'cache': false,
			'dataType': "jsonp",
			'jsonpCallback': 'jsonp_heads',
			'success': function(data) {
				var $ul = $('#chapters_page > ul');
				
				$ul.html('');
				
				for(var json_page in data) {
					$ul.append("<li data-file='"+data[json_page]+"'>"+json_page+"</li>");
				}
				
				$('#chapters_page > ul li').on('click', function() {
					openChapter($(this).data('file'));
				});
			},
			'error': function(xhr, errorType, error) {
				console.log(xhr, errorType, error);
			}
		});	
	}
	
	$('#language_box').on('change', function() {
		changeLang($(this).data('data'));
		setHeads();
		$('#popup_box_wrapper').hide();
	});
	setHeads();
	 
	/**
	 * 
	 */
	function openChapter(file) {
		
		var dir = tizen.filesystem;
		
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == 4 ) {
				console.log("statusText"+xmlhttp.responseXML);
				console.log(xmlhttp.responseXML.firstChild.tagName);
			}
			else {
				console.log("--------response"+ xmlhttp.readyState );
			}
		}
		xmlhttp.open("GET", 'resources/' + localStorage["language"]  + '/' + file, true);
		xmlhttp.send();
		
		return;
		
		 $.ajaxJSONP({
			'url': 'resources/' + localStorage["language"]  + '/' + file,
			'dataType': "jsonp",
			'cache': false,
			'jsonpCallback': 'jsonp_chapter',
			'success': function(data) {
				var $next_page = $('#chapter');
				
				// clear page
				$next_page.empty();
				
				for(var json_position in data) {
					$next_page.append("<p data-position = '"+json_position+"'><span> "+json_position+". </span> "+data[json_position]+"</p>");
				}
				
				var this_number = parseInt($this_page.attr('number'));
				var next_number = parseInt($next_page.attr('number'));
				var down_number = this_number - next_number;
				
				$next_page.css('visibility', 'visible');
				$this_page.css('visibility', 'hidden');
				
				$margin_container.css('margin-left', (down_number + 1) * 360).animate({'margin-left':-next_number * 360}, 500, 'ease')
				
				back_stack.push($this_page);
				$this_page = $next_page;
				$back_button.css('display', 'inline-block');
			}
		});		
	}
	
	$('#chapter').on('click', function() {
		if(e.target.tagName.toLowerCase() == 'p') {
			alert('�� ������ ������� �������� ���?');
		}
	});
};


// window.onload can work without <body onload="">
window.onload = init;

