function changeLang(type) { /* en|ru */
	function change (lang) {
		localStorage["language"] = lang;
		
		$.ajaxJSONP({
			'url': 'resources/language_' + lang + '.json',
			'dataType': "jsonp",
			'cache': false,
			'jsonpCallback': 'jsonp',
			'success': function(data) {
				for(var json_data in data) {
					$('#language_' + json_data).html(data[json_data]);
				}				
			}
		});	
	}

	if(localStorage["language"] == 'undefined' && type == undefined) {
		tizen.systeminfo.getPropertyValue("LOCALE", function(locale) {
			if(/en/i.test(locale.language)) {
				localStorage["language"] = 'en';
			}
			else {
				localStorage["language"] = 'ru';
			}
			change(localStorage["language"]);
		}, function(){});			
	}
	else {
		if(type == undefined) {
			change(localStorage["language"]);
		}
		else {
			change(type);
		}
	}
}