function runner() {
	var last_runner = false;

	$('.runner_container').on(touchStartEvent, function(e) {
		var $this = last_runner = $(this);
		runner_start = true;
		
		$this.data('runner_positions', $this.data('steps'));
		$this.data('runner_step', runner_width/$this.data('steps').split(',').length);
		
		var positions = $this.data('steps').split(',');
		
		var steps = $this.data('runner_step');
		
		for(var i=0; i < positions.length; i++) {
			if((e.pageX-70) > i * steps - steps / 2 && (e.pageX-70) < (i+1) * steps + steps / 2) {
				if(i == positions.length - 1) {
					$('.runner_button', this).css('margin-left', i * steps + 15).html(positions[i]);
				}
				else {
					$('.runner_button', this).css('margin-left', i * steps).html(positions[i]);
				}
			}
		}
		return false;
	})
	.on(touchMoveEvent, function(e) {
		if(runner_start == true) {
			var $this = $(this);
			var steps = $this.data('runner_step');
			var positions = $this.data('runner_positions').split(',');
			
			for(var i=0; i < positions.length; i++) {
				if((e.pageX-70) > i * steps - steps / 2 && (e.pageX-70) < (i+1) * steps + steps / 2) {
					if(i == positions.length - 1) {
						$('.runner_button', this).css('margin-left', i * steps + 15).html(positions[i]);
					}
					else {
						$('.runner_button', this).css('margin-left', i * steps).html(positions[i]);
					}
				}
			}
		}
		return false;
	})


	$(document).on(touchStopEvent, function(e) {
		runner_start = false;
		if(last_runner) {
			last_runner.trigger('change');
			last_runner = false;
		}
		return false;
	});
}